import {inject} from 'aurelia-framework';
import {GridOptions, GridApi, ColumnApi} from "ag-grid";
import {DialogService} from 'aurelia-dialog';

//encapsulated interface to database server
import {DataAccess, TruckLoadDetail} from './data-access';

//modal popup for modifying grid row
import {Prompt} from './components/modal/row-modal';

@inject(DataAccess, DialogService)
export class MyGridPage {

  public gridOptions: GridOptions;
  private api: GridApi;
  private columnApi: ColumnApi;
  private data: DataAccess;
  private service: DialogService;
  private currentRow: TruckLoadDetail = null;

  constructor(data: DataAccess, service: DialogService) {
    this.gridOptions = <GridOptions>{};
    this.gridOptions.columnDefs = [
      {headerName: 'Truck Load ID', field: 'idtruckloads'},
      {headerName: 'Driver', field: 'driver'},
      {headerName: 'Destination', field: 'destination'},
      {headerName: 'ETA', field: 'eta'}];
    this.gridOptions.rowData = [];
    this.data = data;
    this.service = service;
  }

  refreshGrid = ()=>{
    this.data.getAllRows()
        .then(response => {
          this.gridOptions.rowData = response
        });
  }

  onGridReady = () => {
    this.api = this.gridOptions.api;
    this.columnApi = this.gridOptions.columnApi;
    this.refreshGrid();
  }

  onRowClicked = (row: TruckLoadDetail) => {
    this.currentRow = row;
  }

  addEntry = () => {
    console.log('addEntry', this.currentRow);
    this.service.open({viewModel: Prompt, model: {action: 'Add', truckload: null}})
        .then(response => {
          console.log('response', response);
          if(!response.wasCancelled) {
            this.data.addRow(response.output)
                .then(response => {
                  this.refreshGrid();
                }, error => {
                  console.log('error:', error);
                  alert(error);
                });
          }
        })
  }
  deleteEntry = () => {
    console.log('deleteEntry', this.currentRow);
    if (!this.currentRow) {
      alert('Please select row to delete')
    }
    else {
      if (confirm('Delete selected row?')) {
        this.data.deleteRow(this.currentRow)
            .then(response => {
                  console.log('deketeEntry response', response)
                  this.refreshGrid();
                },
                error => {
                  console.log('deleteRow error', error);
                  alert(error);
                })
      }
    }
  }

  editEntry = () => {
    console.log('editEntry');
    if (!this.currentRow) {
      alert('Please select row to edit')
    }
    else {
      this.service.open({viewModel: Prompt, model: {action:'Edit', truckload:this.currentRow}}).then(response => {
        console.log('response', response);
        if(!response.wasCancelled) {
          this.data.updateRow(response.output)
              .then(response => {
                console.log('updateRow response:', response);
                this.refreshGrid();
              }, error => {
                console.log('error:', error);
                alert(error);
              });
        }
      })
    }
  }
}


