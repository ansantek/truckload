/**
 * Created by larry on 4/7/17.
 */
import {autoinject} from 'aurelia-framework';
import {HttpClient, json} from 'aurelia-fetch-client';

export interface TruckLoadDetail {
    idtruckloads: string;
    driver: string;
    destination: string;
    eta: string;
    agent: string;
    timestamp: string;
};

export type TruckLoadDetailArray=TruckLoadDetail[];
export type TruckLoadDetailRow=TruckLoadDetail;

const target = 'http://174.138.53.29:3002'

@autoinject()
export class DataAccess {
    constructor (private http:HttpClient){
        http.configure(config => {
            config
                .withBaseUrl(target)
                .withDefaults({
                    credentials: 'omit',
                    headers: {
                        'Accept': 'application/json',
                        'X-Requested-With': 'Fetch'
                    }
                })
                .withInterceptor({
                    request(request) {
                        console.log(`Requesting ${request.method} ${request.url}`);
                        return request;
                    },
                    response(response) {
                        console.log(`Received ${response.status} ${response.url}`);
                        return response;
                    }
                });
        });
    };

    //database storage format conversion to presentation format
    fromISOToLocal(field:string):string {
        return field.substring(0,10)+' '+field.substring(11,16);
    }

    getAllRows():Promise<TruckLoadDetailArray>{
        console.log('getAllRows http:', this.http);
        return new Promise<TruckLoadDetailArray>(resolve=>{
            this.http.fetch('/SingleTable',{mode:'cors'})
                .then(response=>{
                    response.json().then(data=>{
                        console.log('data:',data);
                        for(var i=0; i<data.length; i++){
                            //convert database storage format to presentation format
                            data[i].eta=this.fromISOToLocal(data[i].eta);
                        }
                        resolve(data);
                    })
                })
            }

        )}

    deleteRow(row:TruckLoadDetail):Promise<TruckLoadDetailRow>{
        console.log('deleteRow', row);
        return new Promise<TruckLoadDetailRow>((resolve,reject)=>{
            this.http.fetch('/SingleTable',{
                mode:'cors',
                method:'delete',
                body:json(row)
            })
                .then(response=>{
                    console.log('delete response;',response);
                    if(response.status != 200){
                        console.log('delete reject');
                        response.json().then(body=>reject(new Error(body.code)))
                    }
                    else{
                        console.log('delete accept' )
                        resolve(response.json())}
                })
        });
    }

    updateRow(row:TruckLoadDetail):Promise<TruckLoadDetailRow>{
        console.log('updateRow:',row);
        return new Promise<TruckLoadDetailRow>((resolve,reject)=>{
            this.http.fetch('/SingleTable',{
                mode:'cors',
                method:'put',
                body:json(row)})
                .then(response=>{
                    if(response.status != 200){
                        response.json().then(body=>reject(new Error(body.code)))
                    }
                    else {
                        resolve(response.json())
                    };
                })
            })
    }

    addRow(row:TruckLoadDetail):Promise<TruckLoadDetailRow>{
        console.log('addRow:', row);
        if (!row.eta){row.eta='2020-01-01'};
        return new Promise<TruckLoadDetailRow>((resolve,reject)=>{
            this.http.fetch('/SingleTable', {
                mode:'cors',
            method:'post',
            body:json(row)})
                .then(response=>{
                    if (response.status != 200){
                        response.json().then(body=>reject(new Error(body.code)))
                    }
                    else {resolve(response.json())};
                })
        })

    }

}
