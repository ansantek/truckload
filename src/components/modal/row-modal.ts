/**
 * Created by larry on 4/17/17.
 */
import {autoinject} from 'aurelia-framework';
import {DialogController} from 'aurelia-dialog';
import {TruckLoadDetail} from "../../data-access";


export interface PromptParameter {
  action: string;
  truckload : TruckLoadDetail;
}

@autoinject()
export class Prompt {
    private truckLoad : TruckLoadDetail;
    private action: string;
    constructor(private controller:DialogController){
        controller.settings.centerHorizontalOnly = true;
    }

    activate(param:PromptParameter) {
        this.action=param.action;
        if(param.truckload){this.truckLoad=param.truckload;}
        else {
            this.truckLoad={
              idtruckloads: '',
              driver: '',
              destination: '',
              eta: '',
              agent: '',
              timestamp: ''};
        }
    }
}