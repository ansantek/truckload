# README #

Aurelia based Truck Load Management demo

###Description ###

This is the Aurelia frontend portion of an application that manages the 
dispatching of truck load shipping.  The backend is implemented as a 
nodejs/expressjs API application, and is located at 
https://bitbucket.org/ansantek/singletablerestfulapi

The operating demo is located at http://174.138.53.29:3002

### Database Access ###

The database accesses required are implemented in an encapsulated module, DataAccess.ts,
and makes use of the Aurelia Fetch component to access the API implemented on the
server.  The underlying database is MySQL.

The database entry for each truck load contains fields for:

Field Name | Field Description
---------- | --------------------------------------
idtruckload | unique id
driver | name of driver
destination | name of destination
eta | estimated time  of arrival format: yyyy-mm-dd hh:mm
agent | name of agent making last change
timestamp | date and time of last change

The timestamp field is included in order to implement a conflict resolution
whenever two agents attempt to edit the same row at the same time.


### Application structure and components ###

The application is basically a single page application without routing.  A popular
grid component, ag-grid, is used to implement the grid.  Modal popup windows are
used via aurelia-dialog to provide adding, editing, and deleting functions.
